<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';
$zbp->Load();
$action='root';
if (!$zbp->CheckRights($action)) {$zbp->ShowError(6);die();}
if (!$zbp->CheckPlugin('Zblog_Eloquent_ORM')) {$zbp->ShowError(48);die();}

$act = GetVars('act', 'GET', 'explain');

$operate = GetVars('operate', 'POST');
if ($operate) {
    CheckIsRefererValid();
    switch ($operate) {
        case 'myisam_to_innodb':
            Zblog_Eloquent_ORM_Myisam_To_Innodb();
            break;
        case 'innodb_to_myisam':
            Zblog_Eloquent_ORM_Innodb_To_Myisam();
            break;
        case 'collation_to_utf8mb4':
            Zblog_Eloquent_ORM_Collation_To_Utf8Mb4();
            break;
    }
    redirect($zbp->fullcurrenturl);
}

if (GetVars('change', 'POST') === 'engine') {
    CheckIsRefererValid();
    $name = GetVars('name', 'POST');
    $engine = GetVars('engine', 'POST');
    Zblog_Eloquent_ORM_ChangeEngine($name, $engine);
    echo json_encode(['code' => 200]);die;
}

if (GetVars('formType', 'POST') === 'mysql') {
    CheckIsRefererValid();
    $mysql = $zbp->Config('Zblog_Eloquent_ORM')->mysql;
    $keyName = GetVars('key', 'POST');
    if (!empty($keyName)) {
        $key = str_replace('key_', '', $keyName);
        unset($mysql[$key]);
    }
    if (empty($mysql)) {
        $newMysql = [$_POST];
    } else {
        $newMysql = array_values(array_merge($mysql, [$_POST]));
    }
    $zbp->Config('Zblog_Eloquent_ORM')->mysql = $newMysql;
    $zbp->SaveConfig('Zblog_Eloquent_ORM');
    echo json_encode(['code' => 200]);die;
}

$delMysql = GetVars('delMysql', 'POST');
if ($delMysql) {
    CheckIsRefererValid();
    $mysql = $zbp->Config('Zblog_Eloquent_ORM')->mysql;
    $key = str_replace('key_', '', $delMysql);
    unset($mysql[$key]);
    $zbp->Config('Zblog_Eloquent_ORM')->mysql = array_values($mysql);
    $zbp->SaveConfig('Zblog_Eloquent_ORM');
    echo json_encode(['code' => 200]);die;
}

$dataBases = Zblog_Eloquent_ORM_Get_Databases();

$blogtitle='Zblog_Eloquent_ORM';
require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';
?>
<div id="divMain">
  <div class="divHeader"><?php echo $blogtitle;?></div>
    <div class="SubMenu">
        <?php Zblog_Eloquent_ORM_SubMenu($act);?>
        <a href="https://www.songhaifeng.com/" target="_blank"><span class="m-left" style="color:#F00">帮助</span></a>
    </div>
  <div id="divMain2">
    <?php if ($act === 'database') { ?>
      <form enctype="multipart/form-data" method="post" action="<?php echo $zbp->fullcurrenturl; ?>" id="myform">
          <input type="hidden" name="csrfToken" value="<?php echo $zbp->GetCsrfToken(); ?>">
          <input id="operate" name="operate" type="hidden" value="" />
          <p>
              <input type="submit" class="button" onclick="$('#operate').val('myisam_to_innodb');" value="批量 - 表引擎MyISAM转InnoDB" />
              <input type="submit" class="button" onclick="$('#operate').val('innodb_to_myisam');" value="批量 - 表引擎InnoDB转MyISAM" />
              <input type="submit" class="button" onclick="$('#operate').val('collation_to_utf8mb4');" value="批量 - 表编码转为utf8mb4" />
          </p>
          <table class="tableFull tableBorder">
              <tr>
                  <th class="td10">数据库表</th>
                  <th>记录条数</th>
                  <th>占用空间</th>
                  <th>表编码</th>
                  <th>表引擎</th>
                  <th>修改时间</th>
                  <th>说明</th>
              </tr>
              <?php
                  foreach ($dataBases as $item) {
                      if ($item['Engine'] === 'InnoDB') {
                          $engineHtml = '<button onclick="changeEngine(\''.$item['Name'].'\', \'MyISAM\')" style="border: 0;background: unset;cursor: pointer;display: inline-block;margin-left: 10px;color: #3399cc;" type="button">[转MyISAM]</button>';
                      } else {
                          $engineHtml = '<button onclick="changeEngine(\''.$item['Name'].'\', \'InnoDB\')" style="border: 0;background: unset;cursor: pointer;display: inline-block;margin-left: 10px;color: #3399cc;" type="button">[转InnoDB]</button>';
                      }
                      echo '<tr>';
                      echo '<td class="td15">'.$item['Name'].'</td>';
                      echo '<td>'.$item['Rows'].'</td>';
                      echo '<td>'.Zblog_Eloquent_ORM_Format_Bytes($item['Data_length']).'</td>';
                      echo '<td>'.$item['Collation'].'</td>';
                      echo '<td>'.$item['Engine'].$engineHtml.'</td>';
                      echo '<td class="td20">'.$item['Create_time'].'</td>';
                      echo '<td class="td10">'.$item['Comment'].'</td>';
                      echo '</tr>';
                  }
              ?>
          </table>
      </form>
        <script>
            $('#myform').submit(function () {
                return confirm("建议先备份数据库后再操作；\r\n表占用空间越大，执行时间越久；\r\n请选择在网站流量较小时执行；\r\n您确认进行此操作吗？") === true;
            })
            function changeEngine(name, engine) {
                if (!confirm("该操作将 "+ name +" 的表引擎修改为 "+ engine +"\r\n您确认进行此操作吗？")) {
                    return false;
                }
                $.ajax({
                    url: '<?php echo $zbp->fullcurrenturl; ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        change: 'engine',
                        name: name,
                        engine: engine,
                        csrfToken: '<?php echo $zbp->GetCsrfToken(); ?>'
                    },
                    success: function() {
                        window.location.href = '<?php echo $zbp->fullcurrenturl; ?>';
                    }
                });
            }
        </script>
    <?php } elseif ($act === 'mysql') { ?>
        <style>
            #divMain2 {margin-bottom: 50px}
            #divMain2 p {padding: 0}
            #divMain2 input[type=button] {margin-top: 10px}
            #divMain2 .form-content {display: inline-block;margin: 0 20px;}
        </style>
        <div class="form-content">
            <form method="post" action="#">
                <input type="hidden" name="csrfToken" value="<?php echo $zbp->GetCsrfToken(); ?>">
                <input type="hidden" name="formType" value="mysql">
                <p>
                    <span class="title">唯一标识:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="ident" type="text">
                </p>
                <p>
                    <span class="title">驱动:</span>
                    <span class="star">(*)</span>
                    <br>
                    <select class="edit" name="driver">
                        <option value="mysql" selected="selected">mysql</option>
                        <option value="mysqli">mysqli</option>
                        <option value="pdo_mysql">pdo_mysql</option>
                        <option value="sqlite">sqlite</option>
                        <option value="sqlite3">sqlite3</option>
                        <option value="pdo_sqlite">pdo_sqlite</option>
                        <option value="postgresql">postgresql</option>
                        <option value="pdo_postgresql">pdo_postgresql</option>
                    </select>
                </p>
                <p>
                    <span class="title">数据库地址:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="host" type="text">
                </p>
                <p>
                    <span class="title">数据库名:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="database" type="text">
                </p>
                <p>
                    <span class="title">用户名:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="username" type="text">
                </p>
                <p>
                    <span class="title">密码:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="password" type="text">
                </p>
                <p>
                    <span class="title">字符集:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="charset" type="text" value="utf8mb4">
                </p>
                <p>
                    <span class="title">端口:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="port" type="text" value="3306">
                </p>
                <p>
                    <span class="title">排序规则:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="collation" type="text" value="utf8mb4_general_ci">
                </p>
                <p>
                    <span class="title">前缀:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="prefix" type="text" value="zbp_">
                </p>
                <p><input type="button" class="button" onclick="mysql(this)" value="保存"></p>
            </form>
        </div>
        <?php if (!empty($zbp->Config('Zblog_Eloquent_ORM')->mysql)) {
            foreach ($zbp->Config('Zblog_Eloquent_ORM')->mysql as $key => $item) { ?>
        <div class="form-content">
            <form method="post" action="#">
                <input type="hidden" name="csrfToken" value="<?php echo $zbp->GetCsrfToken(); ?>">
                <input type="hidden" name="formType" value="mysql">
                <input type="hidden" name="key" value="key_<?php echo $key; ?>">
                <p>
                    <span class="title">唯一标识:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="ident" type="text" value="<?php echo $item['ident'] ?>">
                </p>
                <p>
                    <span class="title">驱动:</span>
                    <span class="star">(*)</span>
                    <br>
                    <select class="edit" name="driver">
                        <option value="mysql" <?php echo $item['driver'] === 'mysql' ? 'selected="selected"' : '' ?>>mysql</option>
                        <option value="mysqli" <?php echo $item['driver'] === 'mysqli' ? 'selected="selected"' : '' ?>>mysqli</option>
                        <option value="pdo_mysql" <?php echo $item['driver'] === 'pdo_mysql' ? 'selected="selected"' : '' ?>>pdo_mysql</option>
                        <option value="sqlite" <?php echo $item['driver'] === 'sqlite' ? 'selected="selected"' : '' ?>>sqlite</option>
                        <option value="sqlite3" <?php echo $item['driver'] === 'sqlite3' ? 'selected="selected"' : '' ?>>sqlite3</option>
                        <option value="pdo_sqlite" <?php echo $item['driver'] === 'pdo_sqlite' ? 'selected="selected"' : '' ?>>pdo_sqlite</option>
                        <option value="postgresql" <?php echo $item['driver'] === 'postgresql' ? 'selected="selected"' : '' ?>>postgresql</option>
                        <option value="pdo_postgresql" <?php echo $item['driver'] === 'pdo_postgresql' ? 'selected="selected"' : '' ?>>pdo_postgresql</option>
                    </select>
                </p>
                <p>
                    <span class="title">数据库地址:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="host" type="text" value="<?php echo $item['host'] ?>">
                </p>
                <p>
                    <span class="title">数据库名:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="database" type="text" value="<?php echo $item['database'] ?>">
                </p>
                <p>
                    <span class="title">用户名:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="username" type="text" value="<?php echo $item['username'] ?>">
                </p>
                <p>
                    <span class="title">密码:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="password" type="text" value="<?php echo $item['password'] ?>">
                </p>
                <p>
                    <span class="title">字符集:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="charset" type="text" value="<?php echo $item['charset'] ?>">
                </p>
                <p>
                    <span class="title">端口:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="port" type="text" value="<?php echo $item['port'] ?>">
                </p>
                <p>
                    <span class="title">排序规则:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="collation" type="text" value="<?php echo $item['collation'] ?>">
                </p>
                <p>
                    <span class="title">前缀:</span>
                    <span class="star">(*)</span>
                    <br>
                    <input class="edit" name="prefix" type="text" value="<?php echo $item['prefix'] ?>">
                </p>
                <p>
                    <input type="button" class="button" onclick="mysql(this)" value="更新">
                    <input type="button" class="button" onclick="delMysql('<?php echo 'key_'.$key; ?>')" style="background: red; border-color: red" value="删除">
                </p>
            </form>
        </div>
    <?php } } ?>
        <script>
            function mysql(object) {
                $.ajax({
                    url: '<?php echo $zbp->fullcurrenturl; ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: $(object).parents('form').serialize(),
                    success: function() {
                        window.location.href = '<?php echo $zbp->fullcurrenturl; ?>';
                    }
                });
            }
            function delMysql(key) {
                if (!confirm("将删除该条数据库配置信息\r\n您确认进行此操作吗？")) {
                    return false;
                }
                $.ajax({
                    url: '<?php echo $zbp->fullcurrenturl; ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        delMysql: key,
                        csrfToken: '<?php echo $zbp->GetCsrfToken(); ?>'
                    },
                    success: function() {
                        window.location.href = '<?php echo $zbp->fullcurrenturl; ?>';
                    }
                });
            }
        </script>
    <?php } else { ?>
        <table class="tableFull tableBorder">
            <tr><th>插件说明</th></tr>
            <tr>
                <td>码云地址：<a href="https://gitee.com/SongHaiFeng/Zblog-Eloquent-ORM" target="_blank">https://gitee.com/SongHaiFeng/Zblog-Eloquent-ORM</a></td>
            </tr>
            <tr>
                <td>使用说明：<a href="https://gitee.com/SongHaiFeng/Zblog-Eloquent-ORM/blob/master/DOC.md" target="_blank">https://gitee.com/SongHaiFeng/Zblog-Eloquent-ORM/blob/master/DOC.md</a></td>
            </tr>
            <tr>
                <td>在熟悉 命名空间 和 composer 及 自动加载 的前提下，使用方法可以参考Laravel的Eloquent ORM</td>
            </tr>
            <tr>
                <td>不支持门面类使用，事务相关请看插件中 Models/BaseModel 的方法，除此之外其它操作不变。</td>
            </tr>
            <tr>
                <td>如有问题可提 <a href="https://gitee.com/SongHaiFeng/Zblog-Eloquent-ORM/issues" target="_blank">Issue</a> ，或者
                    <a href="http://wpa.qq.com/msgrd?v=3&uin=284204003&site=qq&menu=yes" target="_blank">跟我交流</a> ，有其它想法也可以发起
                    <a href="https://gitee.com/SongHaiFeng/Zblog-Eloquent-ORM/pulls" target="_blank">Pull Requests</a> 。</td>
            </tr>
        </table>
    <?php } ?>
  </div>
</div>

<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>