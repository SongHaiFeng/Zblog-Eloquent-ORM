## 使用说明

### 需要准备：

- php版本 `大于等于` 7.4

- 配置好 `composer`

- 熟悉 `命名空间` 和 `自动加载`

### 食用例子：

1、主题或插件内任意目录下新建 `composer.json` 文件，写入以下代码：

```
{
  "autoload": {
    "psr-4": {
      "ModelsDemo\\": "./ModelsDemo",
      "ObserverDemo\\": "./ObserverDemo"
    }
  }
}
```

> 需要注意：为了避免冲突，自动加载的命名空间和文件夹名称带上主题或插件的ID

2、执行 `composer dump-autoload` 配置自动加载

3、在主题或插件的 `include.php` 中引入 `autoload.php`，如：

```
include_once dirname(__FILE__) . DIRECTORY_SEPARATOR.'/vendor/autoload.php';
```

### 关于分页

例子如下：

```
$post = \Models\Post::query()->paginate(10, ['*'], 'p', GetVars('p', 'GET', 1));
echo \Base\Pagination::paginate($post);
```

此处返回为符合 `bootstrap` 的分页 `html结构` ，如返回 `空` 则不存在分页。

另外，`paginate` 方法默认的 `pageName` 为 `page`，而 `zblog` 默认的分页参数也是 `page`，所以此处需要指定分页参数为 `p`，而且必须为`p`。

### 关于事务

原先在 `BaseModel` 中有 `transactionStart()` 、 `transactionCommit()` 、 `transactionRollBack()` 3个关于事务的方法（仅限获取默认的default连接），但现已不推荐使用。

现在新增了 `Zblog_Eloquent_ORM_Get_Connection($name = '')` 方法获取指定链接（默认获取default连接）的 `\Illuminate\Database\Connection`，使操作更加便捷。

可使用：

```php
try {
    // 启用事务
    Zblog_Eloquent_ORM_Get_Connection()->beginTransaction();

    // do something...

    // 提交事务
    Zblog_Eloquent_ORM_Get_Connection()->commit();
} catch (\Exception $exception) {
    // 事务回滚
    Zblog_Eloquent_ORM_Get_Connection()->rollBack();
}
```

也可使用：

```php
Zblog_Eloquent_ORM_Connection()->transaction(function () {
    // do something...
});
```

其实以上都是 `illuminate/database` 的方法，熟悉的朋友可以更加方便的使用相关方法啦~

### 其它

新增了全局 `$zbp->Zblog_Eloquent_ORM` 属性获取 `\Illuminate\Database\Capsule\Manager` 实例，`Zblog_Eloquent_ORM_Get_Connection($name = '')` 也是由此而来。






