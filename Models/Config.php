<?php

namespace Models;

use Models\BaseModel;
use Observers\ConfigObserver;

class Config extends BaseModel
{
    /**
     * 表名
     * @var string
     */
    protected $table = 'config';

    /**
     * 默认连接
     * @var string
     */
    protected $connection = 'default';

    /**
     * 主键ID字段
     * @var string
     */
    protected $primaryKey = 'conf_ID';

    /**
     * 是否维护时间戳
     * @var bool
     */
    public $timestamps = false;

    /**
     * 可以被批量赋值的属性。
     * @var array
     */
    protected $fillable = [
        'conf_Name',
        'conf_Value',
        'conf_Key',
    ];

    public static function boot()
    {
        parent::boot();
        static::observe(new ConfigObserver());
    }

    /**
     * 获取conf_Value
     *
     * @param string $value
     * @return array
     */
    public function getConfValueAttribute($value): array
    {
        return empty($value) ? [] : unserialize($value);
    }

    /**
     * 设置ul_Meta
     *
     * @param string $value
     * @return string
     */
    public function setConfValueAttribute($value): string
    {
        return $this->attributes['conf_Value'] = empty($value) ? '' : serialize($value);
    }
}
