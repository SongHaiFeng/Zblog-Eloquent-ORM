<?php

namespace Models;

use Models\BaseModel;
use Observers\TagObserver;

class Tag extends BaseModel
{
    /**
     * 表名
     * @var string
     */
    protected $table = 'tag';

    /**
     * 默认连接
     * @var string
     */
    protected $connection = 'default';

    /**
     * 主键ID字段
     * @var string
     */
    protected $primaryKey = 'tag_ID';

    /**
     * 是否维护时间戳
     * @var bool
     */
    public $timestamps = false;

    /**
     * 可以被批量赋值的属性。
     * @var array
     */
    protected $fillable = [
        'tag_Name',
        'tag_Order',
        'tag_Type',
        'tag_Count',
        'tag_Alias',
        'tag_Intro',
        'tag_Template',
        'tag_Meta',
        'tag_Group',
    ];

    public static function boot()
    {
        parent::boot();
        static::observe(new TagObserver());
    }

    /**
     * 获取tag_Meta
     *
     * @param string $value
     * @return array
     */
    public function getTagMetaAttribute($value): array
    {
        return empty($value) ? [] : unserialize($value);
    }

    /**
     * 设置tag_Meta
     *
     * @param string $value
     * @return string
     */
    public function setTagMetaAttribute($value): string
    {
        return $this->attributes['tag_Meta'] = empty($value) ? '' : serialize($value);
    }
}
