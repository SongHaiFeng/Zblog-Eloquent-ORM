<?php

namespace Models;

use Models\BaseModel;
use Observers\UploadObserver;

class Upload extends BaseModel
{
	/**
     * 表名
     * @var string
     */
    protected $table = 'upload';

    /**
     * 默认连接
     * @var string
     */
    protected $connection = 'default';

    /**
     * 主键ID字段
     * @var string
     */
    protected $primaryKey = 'ul_ID';

    /**
     * 是否维护时间戳
     * @var bool
     */
    public $timestamps = false;

    /**
     * 可以被批量赋值的属性。
     * @var array
     */
    protected $fillable = [
        'ul_AuthorID',
        'ul_Size',
        'ul_Name',
        'ul_SourceName',
        'ul_MimeType',
        'ul_PostTime',
        'ul_DownNums',
        'ul_LogID',
        'ul_Intro',
        'ul_Meta',
    ];

    public static function boot()
    {
        parent::boot();
        static::observe(new UploadObserver());
    }

    /**
     * 获取ul_Meta
     *
     * @param string $value
     * @return array
     */
    public function getUlMetaAttribute($value): array
    {
        return empty($value) ? [] : unserialize($value);
    }

    /**
     * 设置ul_Meta
     *
     * @param string $value
     * @return string
     */
    public function setUlMetaAttribute($value): string
    {
        return $this->attributes['ul_Meta'] = empty($value) ? '' : serialize($value);
    }

    /**
     * 关联用户
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function member(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
    	return $this->hasOne(new Member(), 'mem_ID', 'ul_AuthorID');
    }

    /**
     * 关联文章
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function post(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(new Post(), 'log_ID', 'ul_LogID');
    }
}
