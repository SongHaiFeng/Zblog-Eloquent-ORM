<?php

namespace Models;

use Models\BaseModel;
use Observers\MemberObserver;

class Member extends BaseModel
{
    /**
     * 表名
     * @var string
     */
    protected $table = 'member';

    /**
     * 默认连接
     * @var string
     */
    protected $connection = 'default';

    /**
     * 主键ID字段
     * @var string
     */
    protected $primaryKey = 'mem_ID';

    /**
     * 是否维护时间戳
     * @var bool
     */
    public $timestamps = false;

    /**
     * 可以被批量赋值的属性。
     * @var array
     */
    protected $fillable = [
        'mem_Guid',
        'mem_Level',
        'mem_Status',
        'mem_Name',
        'mem_Password',
        'mem_Email',
        'mem_HomePage',
        'mem_IP',
        'mem_PostTime',
        'mem_Alias',
        'mem_Intro',
        'mem_Articles',
        'mem_Pages',
        'mem_Comments',
        'mem_Uploads',
        'mem_Template',
        'mem_Meta',
        'mem_CreateTime',
        'mem_UpdateTime',
    ];

    public static function boot()
    {
        parent::boot();
        static::observe(new MemberObserver());
    }

    /**
     * 获取mem_Meta
     *
     * @param string $value
     * @return array
     */
    public function getMemMetaAttribute($value): array
    {
        return empty($value) ? [] : unserialize($value);
    }

    /**
     * 设置mem_Meta
     *
     * @param string $value
     * @return string
     */
    public function setMemMetaAttribute($value): string
    {
        return $this->attributes['mem_Meta'] = empty($value) ? '' : serialize($value);
    }

    /**
     * 关联上传
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function upload(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
    	return $this->hasMany(new Upload(), 'ul_AuthorID', 'mem_ID');
    }

    /**
     * 关联文章
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function post(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(new Post(), 'log_AuthorID', 'mem_ID');
    }

    /**
     * 关联评论
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comment(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(new Comment(), 'comm_AuthorID', 'mem_ID');
    }
}
