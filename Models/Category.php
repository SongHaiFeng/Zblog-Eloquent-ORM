<?php

namespace Models;

use Models\BaseModel;
use Observers\CategoryObserver;

class Category extends BaseModel
{
    /**
     * 表名
     * @var string
     */
    protected $table = 'category';

    /**
     * 默认连接
     * @var string
     */
    protected $connection = 'default';

    /**
     * 主键ID字段
     * @var string
     */
    protected $primaryKey = 'cate_ID';

    /**
     * 是否维护时间戳
     * @var bool
     */
    public $timestamps = false;

    /**
     * 可以被批量赋值的属性。
     * @var array
     */
    protected $fillable = [
        'cate_Name',
        'cate_Order',
        'cate_Type',
        'cate_Count',
        'cate_Alias',
        'cate_Intro',
        'cate_RootID',
        'cate_ParentID',
        'cate_Template',
        'cate_LogTemplate',
        'cate_Meta',
        'cate_Group',
    ];

    public static function boot()
    {
        parent::boot();
        static::observe(new CategoryObserver());
    }

    /**
     * 获取cate_Meta
     *
     * @param string $value
     * @return array
     */
    public function getCateMetaAttribute($value): array
    {
        return empty($value) ? [] : unserialize($value);
    }

    /**
     * 设置cate_Meta
     *
     * @param string $value
     * @return string
     */
    public function setCateMetaAttribute($value): string
    {
        return $this->attributes['cate_Meta'] = empty($value) ? '' : serialize($value);
    }

    /**
     * 上级分类
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function categoryParent(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(new Category(), 'cate_ID', 'cate_ParentID');
    }

    /**
     * 顶级分类
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function categoryRoot(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(new Category(), 'cate_ID', 'cate_RootID');
    }
}
