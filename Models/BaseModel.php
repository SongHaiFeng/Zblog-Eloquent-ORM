<?php

namespace Models;

use Illuminate\Database\Eloquent\Builder;
use \Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    /**
     * 事务开始
     *
     * @throws \Throwable
     */
    public static function transactionStart()
    {
        (new BaseModel())->getConnection()->beginTransaction();
    }

    /**
     * 事务回滚
     *
     * @throws \Throwable
     */
    public static function transactionRollBack()
    {
        (new BaseModel())->getConnection()->rollBack();
    }

    /**
     * 事务提交
     *
     * @throws \Throwable
     */
    public static function transactionCommit()
    {
        (new BaseModel())->getConnection()->commit();
    }

    /**
     * 分页查询
     *
     * @return Builder
     */
    public function scopeCustomPaginate($query, $nums = 10, $field = ['*'], $pageName = 'p', $page = 0)
    {
        $nums = empty($nums) ? 10 : $nums;
        $page = empty($page) ? GetVars('p', 'GET', 1) : $page;
        return $query->paginate($nums, $field, $pageName, $page);
    }
}
