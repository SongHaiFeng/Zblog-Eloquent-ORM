<?php

namespace Models;

use Models\BaseModel;
use Observers\ModuleObserver;

class Module extends BaseModel
{
    /**
     * 表名
     * @var string
     */
    protected $table = 'module';

    /**
     * 默认连接
     * @var string
     */
    protected $connection = 'default';

    /**
     * 主键ID字段
     * @var string
     */
    protected $primaryKey = 'mod_ID';

    /**
     * 是否维护时间戳
     * @var bool
     */
    public $timestamps = false;

    /**
     * 可以被批量赋值的属性。
     * @var array
     */
    protected $fillable = [
        'mod_Name',
        'mod_FileName',
        'mod_Content',
        'mod_SidebarID',
        'mod_HtmlID',
        'mod_Type',
        'mod_MaxLi',
        'mod_Source',
        'mod_IsHideTitle',
        'mod_Meta',
    ];

    public static function boot()
    {
        parent::boot();
        static::observe(new ModuleObserver());
    }

    /**
     * 获取mod_Meta
     *
     * @param string $value
     * @return array
     */
    public function getModMetaAttribute($value): array
    {
        return empty($value) ? [] : unserialize($value);
    }

    /**
     * 设置mod_Meta
     *
     * @param string $value
     * @return string
     */
    public function setModMetaAttribute($value): string
    {
        return $this->attributes['mod_Meta'] = empty($value) ? '' : serialize($value);
    }

    /**
     * 获取mod_Content
     *
     * @param string $value
     * @return string
     */
    public function getModContentAttribute($value): string
    {
        global $zbp;
        return str_replace('{#ZC_BLOG_HOST#}', $zbp->host, $value);
    }
}
