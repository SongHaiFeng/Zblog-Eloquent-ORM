<?php

namespace Models;

use Models\BaseModel;
use Observers\PostObserver;

class Post extends BaseModel
{
    /**
     * 表名
     * @var string
     */
    protected $table = 'post';

    /**
     * 默认连接
     * @var string
     */
    protected $connection = 'default';

    /**
     * 主键ID字段
     * @var string
     */
    protected $primaryKey = 'log_ID';

    /**
     * 是否维护时间戳
     * @var bool
     */
    public $timestamps = false;

    /**
     * 可以被批量赋值的属性。
     * @var array
     */
    protected $fillable = [
        'log_CateID',
        'log_AuthorID',
        'log_Tag',
        'log_Status',
        'log_Type',
        'log_Alias',
        'log_IsTop',
        'log_IsLock',
        'log_Title',
        'log_Intro',
        'log_Content',
        'log_PostTime',
        'log_CommNums',
        'log_ViewNums',
        'log_Template',
        'log_Meta',
        'log_CreateTime',
        'log_UpdateTime',
        'log_Thumb',
        'log_FirstImg',
    ];

    public static function boot()
    {
        parent::boot();
        static::observe(new PostObserver());
    }

    /**
     * 获取log_Meta
     *
     * @param string $value
     * @return array
     */
    public function getLogMetaAttribute($value): array
    {
        return empty($value) ? [] : unserialize($value);
    }

    /**
     * 设置log_Meta
     *
     * @param string $value
     * @return string
     */
    public function setLogMetaAttribute($value): string
    {
        return $this->attributes['log_Meta'] = empty($value) ? '' : serialize($value);
    }

    /**
     * 获取log_Tag
     *
     * @param string $value
     * @return array
     */
    public function getLogTagAttribute($value): array
    {
        $s = str_replace('}{', '|', $value);
        $s = str_replace(array('{', '}'), '', $s);
        return explode('|', $s);
    }

    /**
     * 设置log_Tag
     *
     * @param string $value
     * @return string
     */
    public function setLogTagAttribute($value): string
    {
        global $zbp;
        return $this->attributes['log_Tag'] = empty($value) ? '' : $zbp->ConvertTagIDtoString($value);
    }

    /**
     * 获取log_Content
     *
     * @param string $value
     * @return string
     */
    public function getLogContentAttribute($value): string
    {
        global $zbp;
        return str_replace('{#ZC_BLOG_HOST#}', $zbp->host, $value);
    }

    /**
     * 获取log_Thumb
     *
     * @param string $value
     * @return string
     */
    public function getLogThumbAttribute($value): string
    {
        global $zbp;
        return str_replace('{#ZC_BLOG_HOST#}', $zbp->host, $value);
    }

    /**
     * 获取log_FirstImg
     *
     * @param string $value
     * @return string
     */
    public function getLogFirstImgAttribute($value): string
    {
        global $zbp;
        return str_replace('{#ZC_BLOG_HOST#}', $zbp->host, $value);
    }

    /**
     * 关联用户
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function member(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(new Member(), 'mem_ID', 'log_AuthorID');
    }

    /**
     * 关联分类
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(new Category(), 'cate_ID', 'log_CateID');
    }

    /**
     * 获取标签信息
     *
     * @param int $nums
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function tags($nums = 0)
    {
        $tagArr = $this->log_Tag;
        if (count($tagArr) === 1 || $nums === 1) {
            return Tag::query()->where('tag_ID', $tagArr[0])->get();
        }
        $tagObj = Tag::query()->whereIn('tag_ID', $this->log_Tag);
        if ($nums) {
            $tagObj->take($nums);
        }
        return $tagObj->get();
    }
}
