<?php

namespace Observers;

use Models\Config;

class ConfigObserver
{
    /**
     * 监听创建的事件。(已准备好数据但未创建)
     *
     * @param  Config  $config
     * @return void
     */
    public function creating(Config $config)
    {
        //
    }

    /**
     * 监听创建的事件。(已创建)
     *
     * @param  Config  $config
     * @return void
     */
    public function created(Config $config)
    {
        //
    }

    /**
     * 监听更新的事件。(已准备好数据但未更新)
     *
     * @param  Config  $config
     * @return void
     */
    public function updating(Config $config)
    {
        //
    }

    /**
     * 监听更新的事件。(已更新)
     *
     * @param  Config  $config
     * @return void
     */
    public function updated(Config $config)
    {
        //
    }

    /**
     * 监听删除的事件。(已准备好数据但未删除)
     *
     * @param  Config  $config
     * @return void
     */
    public function deleting(Config $config)
    {
        //
    }

    /**
     * 监听删除的事件。(已删除)
     *
     * @param  Config  $config
     * @return void
     */
    public function deleted(Config $config)
    {
        //
    }

    /**
     * 监听保存的事件。(已准备好数据但未保存)
     *
     * @param  Config  $config
     * @return void
     */
    public function saving(Config $config)
    {
        //
    }

    /**
     * 监听保存的事件。(已保存)
     *
     * @param  Config  $config
     * @return void
     */
    public function saved(Config $config)
    {
        //
    }
}
