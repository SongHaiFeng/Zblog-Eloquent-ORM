<?php

namespace Observers;

use Models\Tag;

class TagObserver
{
    /**
     * 监听创建的事件。(已准备好数据但未创建)
     *
     * @param  Tag  $tag
     * @return void
     */
    public function creating(Tag $tag)
    {
        //
    }

    /**
     * 监听创建的事件。(已创建)
     *
     * @param  Tag  $tag
     * @return void
     */
    public function created(Tag $tag)
    {
        //
    }

    /**
     * 监听更新的事件。(已准备好数据但未更新)
     *
     * @param  Tag  $tag
     * @return void
     */
    public function updating(Tag $tag)
    {
        //
    }

    /**
     * 监听更新的事件。(已更新)
     *
     * @param  Tag  $tag
     * @return void
     */
    public function updated(Tag $tag)
    {
        //
    }

    /**
     * 监听删除的事件。(已准备好数据但未删除)
     *
     * @param  Tag  $tag
     * @return void
     */
    public function deleting(Tag $tag)
    {
        global $zbp;
        $zbp->DelItemToNavbar('tag', $tag->tag_ID);
    }

    /**
     * 监听删除的事件。(已删除)
     *
     * @param  Tag  $tag
     * @return void
     */
    public function deleted(Tag $tag)
    {
        global $zbp;
        $zbp->AddBuildModule('tags');
        $zbp->BuildModule();
        $zbp->SaveCache();
    }

    /**
     * 监听保存的事件。(已准备好数据但未保存)
     *
     * @param  Tag  $tag
     * @return void
     */
    public function saving(Tag $tag)
    {
        //
    }

    /**
     * 监听保存的事件。(已保存)
     *
     * @param  Tag  $tag
     * @return void
     */
    public function saved(Tag $tag)
    {
        global $zbp;
        $zbp->AddBuildModule('tags');
        $zbp->BuildModule();
        $zbp->SaveCache();
    }
}
