<?php

namespace Observers;

use Models\Module;

class ModuleObserver
{
    /**
     * 监听创建的事件。(已准备好数据但未创建)
     *
     * @param  Module  $module
     * @return void
     */
    public function creating(Module $module)
    {
        //
    }

    /**
     * 监听创建的事件。(已创建)
     *
     * @param  Module  $module
     * @return void
     */
    public function created(Module $module)
    {
        //
    }

    /**
     * 监听更新的事件。(已准备好数据但未更新)
     *
     * @param  Module  $module
     * @return void
     */
    public function updating(Module $module)
    {
        //
    }

    /**
     * 监听更新的事件。(已更新)
     *
     * @param  Module  $module
     * @return void
     */
    public function updated(Module $module)
    {
        //
    }

    /**
     * 监听删除的事件。(已准备好数据但未删除)
     *
     * @param  Module  $module
     * @return void
     */
    public function deleting(Module $module)
    {
        //
    }

    /**
     * 监听删除的事件。(已删除)
     *
     * @param  Module  $module
     * @return void
     */
    public function deleted(Module $module)
    {
        //
    }

    /**
     * 监听保存的事件。(已准备好数据但未保存)
     *
     * @param  Module  $module
     * @return void
     */
    public function saving(Module $module)
    {
        //
    }

    /**
     * 监听保存的事件。(已保存)
     *
     * @param  Module  $module
     * @return void
     */
    public function saved(Module $module)
    {
        global $zbp;
        $zbp->BuildModule();
        $zbp->SaveCache();
    }
}
