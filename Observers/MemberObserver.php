<?php

namespace Observers;

use Models\Member;

class MemberObserver
{
    /**
     * 监听创建的事件。(已准备好数据但未创建)
     *
     * @param  Member  $member
     * @return void
     */
    public function creating(Member $member)
    {
        //
    }

    /**
     * 监听创建的事件。(已创建)
     *
     * @param  Member  $member
     * @return void
     */
    public function created(Member $member)
    {
        //
    }

    /**
     * 监听更新的事件。(已准备好数据但未更新)
     *
     * @param  Member  $member
     * @return void
     */
    public function updating(Member $member)
    {
        //
    }

    /**
     * 监听更新的事件。(已更新)
     *
     * @param  Member  $member
     * @return void
     */
    public function updated(Member $member)
    {
        //
    }

    /**
     * 监听删除的事件。(已准备好数据但未删除)
     *
     * @param  Member  $member
     * @return void
     */
    public function deleting(Member $member)
    {
        global $zbp;
        DelMember_AllData($member->mem_ID);
        $zbp->AddBuildModule('authors');
        $zbp->BuildModule();
        $zbp->SaveCache();
    }

    /**
     * 监听删除的事件。(已删除)
     *
     * @param  Member  $member
     * @return void
     */
    public function deleted(Member $member)
    {
        //
    }

    /**
     * 监听保存的事件。(已准备好数据但未保存)
     *
     * @param  Member  $member
     * @return void
     */
    public function saving(Member $member)
    {
        //
    }

    /**
     * 监听保存的事件。(已保存)
     *
     * @param  Member  $member
     * @return void
     */
    public function saved(Member $member)
    {
        global $zbp;
        $zbp->AddBuildModule('authors');
        $zbp->BuildModule();
        $zbp->SaveCache();
    }
}
