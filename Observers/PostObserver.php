<?php

namespace Observers;

use Models\Post;

class PostObserver
{
    /**
     * 监听创建的事件。(已准备好数据但未创建)
     *
     * @param  Post  $post
     * @return void
     */
    public function creating(Post $post)
    {
        //
    }

    /**
     * 监听创建的事件。(已创建)
     *
     * @param  Post  $post
     * @return void
     */
    public function created(Post $post)
    {
        //
    }

    /**
     * 监听更新的事件。(已准备好数据但未更新)
     *
     * @param  Post  $post
     * @return void
     */
    public function updating(Post $post)
    {
        //
    }

    /**
     * 监听更新的事件。(已更新)
     *
     * @param  Post  $post
     * @return void
     */
    public function updated(Post $post)
    {
        //
    }

    /**
     * 监听删除的事件。(已准备好数据但未删除)
     *
     * @param  Post  $post
     * @return void
     */
    public function deleting(Post $post)
    {
        global $zbp;
        $zbp->BuildModule();
        $zbp->SaveCache();
    }

    /**
     * 监听删除的事件。(已删除)
     *
     * @param  Post  $post
     * @return void
     */
    public function deleted(Post $post)
    {
        //
    }

    /**
     * 监听保存的事件。(已准备好数据但未保存)
     *
     * @param  Post  $post
     * @return void
     */
    public function saving(Post $post)
    {
        //
    }

    /**
     * 监听保存的事件。(已保存)
     *
     * @param  Post  $post
     * @return void
     */
    public function saved(Post $post)
    {
        global $zbp;
        if ($post->log_Type == ZC_POST_TYPE_ARTICLE) {
            $zbp->AddBuildModule('previous');
            $zbp->AddBuildModule('calendar');
            $zbp->AddBuildModule('comments');
            $zbp->AddBuildModule('archives');
            $zbp->AddBuildModule('tags');
            $zbp->AddBuildModule('authors');
        }
        if ($post->log_Type == ZC_POST_TYPE_PAGE) {
            $zbp->AddBuildModule('comments');
        }
        $zbp->BuildModule();
        $zbp->SaveCache();
    }
}
