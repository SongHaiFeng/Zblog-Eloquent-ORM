<?php

namespace Observers;

use Models\Comment;

class CommentObserver
{
    /**
     * 监听创建的事件。(已准备好数据但未创建)
     *
     * @param  Comment  $comment
     * @return void
     */
    public function creating(Comment $comment)
    {
        //
    }

    /**
     * 监听创建的事件。(已创建)
     *
     * @param  Comment  $comment
     * @return void
     */
    public function created(Comment $comment)
    {
        //
    }

    /**
     * 监听更新的事件。(已准备好数据但未更新)
     *
     * @param  Comment  $comment
     * @return void
     */
    public function updating(Comment $comment)
    {
        //
    }

    /**
     * 监听更新的事件。(已更新)
     *
     * @param  Comment  $comment
     * @return void
     */
    public function updated(Comment $comment)
    {
        //
    }

    /**
     * 监听删除的事件。(已准备好数据但未删除)
     *
     * @param  Comment  $comment
     * @return void
     */
    public function deleting(Comment $comment)
    {
        global $zbp;
        DelComment_Children($comment->comm_ID);
        $zbp->AddBuildModule('comments');
        $zbp->BuildModule();
        $zbp->SaveCache();
    }

    /**
     * 监听删除的事件。(已删除)
     *
     * @param  Comment  $comment
     * @return void
     */
    public function deleted(Comment $comment)
    {
        //
    }

    /**
     * 监听保存的事件。(已准备好数据但未保存)
     *
     * @param  Comment  $comment
     * @return void
     */
    public function saving(Comment $comment)
    {
        //
    }

    /**
     * 监听保存的事件。(已保存)
     *
     * @param  Comment  $comment
     * @return void
     */
    public function saved(Comment $comment)
    {
        global $zbp;
        $zbp->AddBuildModule('comments');
        $zbp->BuildModule();
        $zbp->SaveCache();
    }
}
