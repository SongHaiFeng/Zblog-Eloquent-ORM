<?php

namespace Observers;

use Models\Upload;

class UploadObserver
{
    /**
     * 监听创建的事件。(已准备好数据但未创建)
     *
     * @param  Upload  $upload
     * @return void
     */
    public function creating(Upload $upload)
    {
        //
    }

    /**
     * 监听创建的事件。(已创建)
     *
     * @param  Upload  $upload
     * @return void
     */
    public function created(Upload $upload)
    {
        //
    }

    /**
     * 监听更新的事件。(已准备好数据但未更新)
     *
     * @param  Upload  $upload
     * @return void
     */
    public function updating(Upload $upload)
    {
        //
    }

    /**
     * 监听更新的事件。(已更新)
     *
     * @param  Upload  $upload
     * @return void
     */
    public function updated(Upload $upload)
    {
        //
    }

    /**
     * 监听删除的事件。(已准备好数据但未删除)
     *
     * @param  Upload  $upload
     * @return void
     */
    public function deleting(Upload $upload)
    {
        //
    }

    /**
     * 监听删除的事件。(已删除)
     *
     * @param  Upload  $upload
     * @return void
     */
    public function deleted(Upload $upload)
    {
        //
    }

    /**
     * 监听保存的事件。(已准备好数据但未保存)
     *
     * @param  Upload  $upload
     * @return void
     */
    public function saving(Upload $upload)
    {
        //
    }

    /**
     * 监听保存的事件。(已保存)
     *
     * @param  Upload  $upload
     * @return void
     */
    public function saved(Upload $upload)
    {
        //
    }
}
