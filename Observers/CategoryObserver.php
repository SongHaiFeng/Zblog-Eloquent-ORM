<?php

namespace Observers;

use Models\Category;

class CategoryObserver
{
    /**
     * 监听创建的事件。(已准备好数据但未创建)
     *
     * @param  Category  $category
     * @return void
     */
    public function creating(Category $category)
    {
        //
    }

    /**
     * 监听创建的事件。(已创建)
     *
     * @param  Category  $category
     * @return void
     */
    public function created(Category $category)
    {
        //
    }

    /**
     * 监听更新的事件。(已准备好数据但未更新)
     *
     * @param  Category  $category
     * @return void
     */
    public function updating(Category $category)
    {
        //
    }

    /**
     * 监听更新的事件。(已更新)
     *
     * @param  Category  $category
     * @return void
     */
    public function updated(Category $category)
    {
        //
    }

    /**
     * 监听删除的事件。(已准备好数据但未删除)
     *
     * @param  Category  $category
     * @return void
     */
    public function deleting(Category $category)
    {
        global $zbp;
        DelCategory_Articles($category->cate_ID);
        $zbp->LoadCategories();
        $zbp->AddBuildModule('catalog');
        $zbp->DelItemToNavbar('category', $category->cate_ID);
        $zbp->BuildModule();
        $zbp->SaveCache();
    }

    /**
     * 监听删除的事件。(已删除)
     *
     * @param  Category  $category
     * @return void
     */
    public function deleted(Category $category)
    {
        //
    }

    /**
     * 监听保存的事件。(已准备好数据但未保存)
     *
     * @param  Category  $category
     * @return void
     */
    public function saving(Category $category)
    {
        //
    }

    /**
     * 监听保存的事件。(已保存)
     *
     * @param  Category  $category
     * @return void
     */
    public function saved(Category $category)
    {
        global $zbp;
        $zbp->AddBuildModule('catalog');
        $zbp->BuildModule();
        $zbp->SaveCache();
    }
}
