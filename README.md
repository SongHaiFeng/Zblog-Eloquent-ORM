### Zblog-Eloquent-ORM

来源于 [Laravel](https://laravel.com/) 的 [illuminate/database](https://github.com/illuminate/database)

`zblog` 安装后即可使用，使用方法见 [DOC](./DOC.md) ，操作见 `laravel` 文档 [Eloquent ORM](https://learnku.com/docs/laravel/8.x/eloquent/9406)

已内置 `zblog` 默认的数据表模型 和 模型观察者